CSC     = "$(ProgramW6432) (x86)\Microsoft Visual Studio\2022\BuildTools\MSBuild\Current\Bin\Roslyn\csc.exe"
MSBUILD = "$(ProgramW6432) (x86)\Microsoft Visual Studio\2022\BuildTools\MSBuild\Current\Bin\MSBuild.exe"
VSTEST  = "$(ProgramW6432) (x86)\Microsoft Visual Studio\2022\BuildTools\Common7\IDE\Extensions\TestPlatform\vstest.console.exe"
7Z      = "$(ProgramW6432)\7-Zip\7z.exe"

SRCDIR  = src
SRCREPO = $(SRCDIR)/.git/config
OBJDIR  = $(SRCDIR)/KancolleSniffer/bin/Release

PAGEDIR  = page
PAGEREPO = $(PAGEDIR)/.git/config
VERSION  = $(shell cat $(PAGEDIR)/version)

# System.Data.SQLite source repository
# https://system.data.sqlite.org/index.html/dir

PATCH_ARGS = --binary -s -f -p1 -i

PATCHES = damageHistory2.patch

PACKAGE_FOLDER = KancolleSniffer

RELEASE_FILES = KancolleSniffer.exe \
                DynaJson.dll \
                EnemySlot.csv \
                NumEquips.csv \
                TP.csv \
                minasw.csv \
                tbbonus.csv \
                proxy.pac \
                index.html \
                favicon.ico \
                tags.tag \
                ajax-loader.gif \
                upgrade.ico \
                System.Data.SQLite.dll \
                x64

LICENSE_FILES = LICENSE.txt \
                NOTICE.md \
                README.md

BURAGESNAP = BurageSnap-2.6.zip
YUKARI     = Yukari-2.0.zip
SASARA     = Sasara-1.2.zip


define reverse
$(shell perl -le 'print join " ", reverse @ARGV' $(1))
endef

define apply-patch
if patch --dry-run $(2) $(PATCH_ARGS) $(CURDIR)/$(1) ; then \
  echo apply $(2) $(1) ; \
  patch --no-backup-if-mismatch $(2) $(PATCH_ARGS) $(CURDIR)/$(1) ; \
else \
  echo failed $(2) $(1) ; \
  echo ; \
fi
endef

help:
	@echo $(MAKE) checkout
	@echo $(MAKE) nuget-restore
	@echo $(MAKE) patch
	@echo $(MAKE) unpatch
	@echo $(MAKE) debug
	@echo $(MAKE) test
	@echo $(MAKE) build
	@echo $(MAKE) package
	@echo $(MAKE) clean
	@echo
	@echo VERSION:$(VERSION)
	@echo MSBUILD:$(MSBUILD)
	@echo VSTEST_OPTIONS:$(VSTEST_OPTIONS)
	@echo example: $(MAKE) test VSTEST_OPTIONS=/Tests:KancolleSniffer.Test.ShipStatusTest

patch:
	@cd $(SRCDIR) && $(foreach PATCH,$(PATCHES),$(call apply-patch,$(PATCH));)

unpatch:
	@cd $(SRCDIR) && $(foreach PATCH,$(call reverse,$(PATCHES)),$(call apply-patch,$(PATCH),-R);)


build:
	cd $(SRCDIR) && $(MSBUILD) -m -p:Configuration=Release -t:KancolleSniffer

debug:
	cd $(SRCDIR) && $(MSBUILD) -m -p:Configuration=Debug

test: debug
	cd $(SRCDIR)/KancolleSniffer.Test/bin/Debug && $(VSTEST) KancolleSniffer.Test.dll --Settings:../../.runsettings $(VSTEST_OPTIONS)

clean:
	rm -rf $(SRCDIR)/KancolleSniffer/obj
	rm -rf $(SRCDIR)/KancolleSniffer/bin
	rm -rf $(SRCDIR)/KancolleSniffer.Test/obj
	rm -rf $(SRCDIR)/KancolleSniffer.Test/bin

package: $(BURAGESNAP) $(YUKARI) $(SASARA)
	rm -rf $(PACKAGE_FOLDER)
	mkdir  $(PACKAGE_FOLDER)
	$(foreach FILE,$(RELEASE_FILES),cp -rp $(OBJDIR)/$(FILE) $(PACKAGE_FOLDER);)
	$(foreach FILE,$(LICENSE_FILES),cp -rp $(SRCDIR)/$(FILE) $(PACKAGE_FOLDER);)
	$(7Z) a -mx=9 $(PAGEDIR)/$(PACKAGE_FOLDER)-$(VERSION).min.zip $(PACKAGE_FOLDER)
	sha256sum `find $(PACKAGE_FOLDER) -type f` > $(PAGEDIR)/$(PACKAGE_FOLDER)-$(VERSION).sha256sum

	cp -rp $(OBJDIR)/x86 $(PACKAGE_FOLDER)
	cd $(PACKAGE_FOLDER) && $(7Z) x ../$(BURAGESNAP)
	mv $(PACKAGE_FOLDER)/BurageSnap $(PACKAGE_FOLDER)/Capture

	cp -rp $(SRCDIR)/Sounds/*.mp3 $(PACKAGE_FOLDER)
	mkdir $(PACKAGE_FOLDER)/Yukari
	cd $(PACKAGE_FOLDER)/Yukari && $(7Z) x ../../$(YUKARI)
	cp -rp voice/Yukari/*.mp3 $(PACKAGE_FOLDER)/Yukari
	mkdir $(PACKAGE_FOLDER)/Sasara
	cd $(PACKAGE_FOLDER)/Sasara && $(7Z) x ../../$(SASARA)
	cp -rp voice/Sasara/*.mp3 $(PACKAGE_FOLDER)/Sasara

	$(7Z) a -mx=9 $(PAGEDIR)/$(PACKAGE_FOLDER)-$(VERSION).zip $(PACKAGE_FOLDER)
	cd $(PAGEDIR) && sha256sum $(PACKAGE_FOLDER)-$(VERSION).min.zip $(PACKAGE_FOLDER)-$(VERSION).zip >> $(PACKAGE_FOLDER)-$(VERSION).sha256sum
	cd $(PAGEDIR) && ./genversions.sh


nuget-restore: checkout
	rm -rf $(SRCDIR)/packages
	cd $(SRCDIR) && $(MSBUILD) -t:restore -p:RestorePackagesConfig=true

checkout: $(SRCREPO) $(PAGEREPO)

$(SRCREPO):
	git clone git@bitbucket.org:kancollesniffer/kancollesniffer.git $(SRCDIR)
	cd $(SRCDIR) && git submodule update -i

$(PAGEREPO):
	git clone git@bitbucket.org:kancollesniffer/kancollesniffer.bitbucket.io.git $(PAGEDIR)

$(BURAGESNAP):
	curl -L "https://ja.osdn.net/frs/redir.php?m=dotsrc&f=buragesnap%2F70415%2FBurageSnap-2.6.zip" > $(BURAGESNAP)

$(YUKARI):
	curl -L "https://ja.osdn.net/frs/redir.php?m=ymu&f=kancollesniffer%2F70387%2FYukari-2.0.zip" > $(YUKARI)

$(SASARA):
	curl -L "https://ja.osdn.net/frs/redir.php?m=bfsu&f=kancollesniffer%2F73569%2FSasara-1.2.zip" > $(SASARA)

.PHONY: help patch unpatch build debug test clean package nuget-restore checkout
