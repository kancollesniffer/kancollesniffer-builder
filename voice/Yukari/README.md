結月ゆかりボイス for KancolleSniffer

「VOICEROID2 結月ゆかり」で作成したKancolleSniffer用の音声ファイル集です。

生成した .wav を以下のように編集します。
- ピークを -3.0db として増幅
- 固定ビットレート 64kbps モノラルで mp3 に変換
