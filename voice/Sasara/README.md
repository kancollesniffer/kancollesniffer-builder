さとうささらボイス for KancolleSniffer

KancolleSniffer用の音声ファイル集です。CeVIO Creative Studioの「さとうささら」を使用しています。

生成した .wav を以下のように編集します。
- ピークを -6.0db として増幅
- 先頭と末尾の無音を切り詰め
- 固定ビットレート 64kbps モノラルで mp3 に変換
