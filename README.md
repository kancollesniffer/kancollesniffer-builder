# パッチファイルについて
KancolleSniffer に対して、以下の追加機能を提供します。

- localhost 以外の他のホストからに対してもプロキシ接続を受け付けます。
- ログ出力とログ検索を sqlite3 に置き換えます。csv への出力は維持します。

バージョン 14 で本体に取り込む予定です。

KancolleSniffer については [ウェブサイト](https://kancollesniffer.bitbucket.io/) を参照してください。

ライセンスはApache Licenseバージョン2.0です。
