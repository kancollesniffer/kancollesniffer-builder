using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Globalization;

using System.Data.SQLite;

class BuidSQLite
{
    static void Main(string[] args)
    {
        var path = @"KancolleSniffer.db";
        if (File.Exists(path)) File.Delete(path);

        using (var connection = new SQLiteConnection(new SQLiteConnectionStringBuilder{ DataSource = path }.ToString()))
        {
            connection.Open();

            CreateTable(connection, @"Mission", @"遠征報告書.csv");
            CreateTable(connection, @"Battle", @"海戦・ドロップ報告書.csv");
            CreateTable(connection, @"Material", @"資材ログ.csv");
            CreateTable(connection, @"CreateItem", @"開発報告書.csv");
            CreateTable(connection, @"CreateShip", @"建造報告書.csv");
            CreateTable(connection, @"RemodelSlot", @"改修報告書.csv");
            CreateTable(connection, @"Achievement", @"戦果.csv");
        }
    }

    static void CreateTable(SQLiteConnection connection, string table, string csv)
    {
        using (var transaction = connection.BeginTransaction())
        {
            using (var cmd = new SQLiteCommand(connection))
            {
                cmd.CommandText = $@"create table {table}(timestamp integer, csv text)";
                cmd.ExecuteNonQuery();
                cmd.CommandText = $@"create index {table}Index on {table}(timestamp);";
                cmd.ExecuteNonQuery();

                if (File.Exists(csv))
                {
                    cmd.CommandText = $@"insert into {table}(timestamp, csv) values($timestamp, $csv)";
                    foreach (var line in File.ReadLines(csv, Encoding.GetEncoding("Shift_JIS")).Skip(1))
                    {
                        var timestamp = ToSeconds(line.Split(',')[0]);
                        cmd.Parameters.AddWithValue("$timestamp", timestamp);
                        cmd.Parameters.AddWithValue("$csv", line);
                        cmd.ExecuteNonQuery();
                        cmd.Parameters.RemoveAt("$timestamp");
                        cmd.Parameters.RemoveAt("$csv");
                    }
                }
            }
            transaction.Commit();
        }
    }

    static long ToSeconds(string dateTime)
    {
        return ParseDateTime(dateTime).Ticks / (TimeSpan.TicksPerMillisecond * 1000);
    }

    static long ToJsTimestamp(string dateTime)
    {
        DateTime UNIX_EPOCH = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);

        TimeSpan elapsedTime = ParseDateTime(dateTime) - UNIX_EPOCH;
        return (long) elapsedTime.TotalSeconds * 1000;
    }

    // copy and modify from KancolleSniffer.Log.LogProcessor.ParseDateTime()
    static DateTime ParseDateTime(string dateTime)
    {
        DateTime date;
        int year;

        if (DateTime.TryParseExact(dateTime, @"yyyy\-MM\-dd HH\:mm\:ss", CultureInfo.InvariantCulture,
            DateTimeStyles.AssumeLocal, out date))
        {
            return date;
        }
        // システムが和暦に設定されていて和暦が出力されてしまったケースを救う
        if (dateTime[2] == '-')
        {
            if (!int.TryParse(dateTime.Substring(0, 2), out year))
                return DateTime.Now;
            dateTime = 1988 + year + dateTime.Substring(2);
            return DateTime.TryParseExact(dateTime, @"yyyy\-MM\-dd HH\:mm\:ss", CultureInfo.InvariantCulture,
                DateTimeStyles.AssumeLocal, out date)
                ? date
                : DateTime.Now;
        }
        return DateTime.TryParse(dateTime, CultureInfo.CurrentCulture, DateTimeStyles.AssumeLocal, out date)
            ? date
            : DateTime.Now;
    }
}
